package com.mangoapps.data.local;

/**
 * Created by Akash Bangad on 10/04/2018.
 */

public interface LocalPropertiesDao {

    void setCurrentPage(int page);
    int getCurrentPage();
}
