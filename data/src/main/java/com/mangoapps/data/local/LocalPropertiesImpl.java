package com.mangoapps.data.local;

import android.content.SharedPreferences;

/**
 * Created by Akash Bangad on 10/04/2018.
 */

public class LocalPropertiesImpl implements LocalPropertiesDao {

    public final String CURRENT_PAGE_KEY = "currentPage";
    public static final int CONTACTS_PAGE = 1;
    public static final int CALL_PAGE = 2;
    public static final int SMS_PAGE = 3;
    private final SharedPreferences sharedPreferences;


    public LocalPropertiesImpl(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public int getCurrentPage() {
        return sharedPreferences.getInt(CURRENT_PAGE_KEY, CONTACTS_PAGE);
    }

    @Override
    public void setCurrentPage(int page) {
        sharedPreferences.edit().putInt(CURRENT_PAGE_KEY, page).apply();
    }
}
