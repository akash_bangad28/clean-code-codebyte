package com.mangoapps.data;

import com.mangoapps.data.local.LocalPropertiesDao;
import com.mangoapps.data.provider.contact.ContactDao;
import com.mangoapps.data.provider.phonelog.PhoneLogDao;
import com.mangoapps.data.provider.sms.SmsDao;
import com.mangoapps.domain.model.CallLog;
import com.mangoapps.domain.model.Contact;
import com.mangoapps.domain.model.SmsMessage;
import com.mangoapps.domain.repository.AppRepository;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Akash Bangad on 09/04/2018.
 */

public class AppRepositoryImpl implements AppRepository {

    private final ContactDao contactDao;
    private final PhoneLogDao phoneLogDao;
    private final SmsDao smsDao;
    private final LocalPropertiesDao localPropertiesDao;
    public AppRepositoryImpl(ContactDao contactDao, PhoneLogDao phoneLogDao, SmsDao smsDao, LocalPropertiesDao localPropertiesDao) {
        this.contactDao = contactDao;
        this.phoneLogDao = phoneLogDao;
        this.smsDao = smsDao;
        this.localPropertiesDao = localPropertiesDao;
    }


    @Override
    public Single<List<Contact>> getContactList() {
        return contactDao.getContacts();
    }

    @Override
    public Single<List<CallLog>> getCallLogs() {
        return phoneLogDao.getPhoneLogs();
    }

    @Override
    public Single<List<SmsMessage>> getSmsList() {
        return smsDao.getSmsList();
    }

    @Override
    public void setCurrentPage(int page) {
        localPropertiesDao.setCurrentPage(page);
    }

    @Override
    public int getCurrentPage() {
        return localPropertiesDao.getCurrentPage();
    }
}
