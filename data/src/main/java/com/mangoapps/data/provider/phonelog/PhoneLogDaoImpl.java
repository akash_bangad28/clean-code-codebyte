package com.mangoapps.data.provider.phonelog;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.mangoapps.domain.model.CallLog;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Single;

import static android.content.ContentValues.TAG;

/**
 * Created by Akash Bangad on 09/04/2018.
 */

public class PhoneLogDaoImpl implements PhoneLogDao {


    private final ContentResolver contentResolver;

    public PhoneLogDaoImpl(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;
    }

    @Override
    public Single<List<CallLog>> getPhoneLogs() {
        return  Single.fromCallable(this::fetchPhoneLogs);
    }

    private List<CallLog> fetchPhoneLogs(){
        List<CallLog> phoneLogs = new ArrayList<>();
        Uri allCalls = Uri.parse("content://call_log/calls");
        String strOrder = android.provider.CallLog.Calls.DATE + " DESC";
        Cursor cursor =contentResolver.query(allCalls, null, null, null, strOrder);
        while (cursor!=null && cursor.moveToNext()){
            String number = cursor.getString(cursor.getColumnIndex(android.provider.CallLog.Calls.NUMBER));
            String name = cursor.getString(cursor.getColumnIndex(android.provider.CallLog.Calls.CACHED_NAME));
            String date = cursor.getString(cursor.getColumnIndex(android.provider.CallLog.Calls.DATE));
            String typeCode = cursor.getString(cursor.getColumnIndex(android.provider.CallLog.Calls.TYPE));
            String callType = "";
            int type = Integer.parseInt(typeCode);
            switch (type){
                case android.provider.CallLog.Calls.OUTGOING_TYPE:
                    callType = "Outgoing";
                    break;
                case android.provider.CallLog.Calls.INCOMING_TYPE:
                    callType = "Incoming";
                    break;
                case android.provider.CallLog.Calls.MISSED_TYPE:
                    callType = "Missed";
                    break;
            }
            CallLog callLog = new CallLog();
            callLog.setName(name);
            callLog.setNumber(number);
            callLog.setDate(date);
            callLog.setType(callType);
            phoneLogs.add(callLog);
        }
        cursor.close();
        return phoneLogs;

    }
}
