package com.mangoapps.data.provider.contact;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;

import com.mangoapps.domain.model.Contact;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Single;

/**
 * Created by Akash Bangad on 08/04/2018.
 */

public class ContactDaoImpl implements ContactDao{

   private final ContentResolver contentResolver;

    public ContactDaoImpl(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;
    }

    @Override
    public Single<List<Contact>> getContacts() {
        return Single.fromCallable(this::fetchContactList);
    }

    private List<Contact> fetchContactList(){
        List<Contact> contactList = new ArrayList<>();
        Cursor managedCursor = contentResolver
                .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        new String[] {ContactsContract.CommonDataKinds.Phone._ID,ContactsContract.CommonDataKinds.Phone.PHOTO_URI,ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER}, null, null,  ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        while (managedCursor != null && managedCursor.moveToNext()) {
            String id = managedCursor.getString(managedCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID));
            String name = managedCursor.getString(managedCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String number = managedCursor.getString(managedCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            String thumb = managedCursor.getString(managedCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI));
            String photo = managedCursor.getString(managedCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
            Contact contact = new Contact(id,name,photo,number,thumb);
            contactList.add(contact);
        }
        managedCursor.close();
        return contactList;
    }
}
