package com.mangoapps.data.provider.sms;

import com.mangoapps.domain.model.SmsMessage;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Akash Bangad on 10/04/2018.
 */

public interface SmsDao {
    Single<List<SmsMessage>> getSmsList();
}
