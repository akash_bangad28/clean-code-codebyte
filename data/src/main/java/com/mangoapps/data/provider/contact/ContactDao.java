package com.mangoapps.data.provider.contact;

import com.mangoapps.domain.model.Contact;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Akash Bangad on 08/04/2018.
 */

public interface ContactDao {
    Single<List<Contact>> getContacts();
}
