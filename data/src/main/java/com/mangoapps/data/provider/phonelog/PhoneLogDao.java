package com.mangoapps.data.provider.phonelog;

import com.mangoapps.domain.model.CallLog;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Akash Bangad on 09/04/2018.
 */

public interface PhoneLogDao {

    Single<List<CallLog>> getPhoneLogs();
}
