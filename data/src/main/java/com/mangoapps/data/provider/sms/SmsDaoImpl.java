package com.mangoapps.data.provider.sms;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Telephony;
import android.util.Log;

import com.mangoapps.domain.model.SmsMessage;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;

/**
 * Created by Akash Bangad on 10/04/2018.
 */

public class SmsDaoImpl implements SmsDao {


    public static final String TAG = SmsDaoImpl.class.getSimpleName();
    private final ContentResolver contentResolver;

    public SmsDaoImpl(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;
    }

    @Override
    public Single<List<SmsMessage>> getSmsList() {
        return Single.fromCallable(this::fetchSmsList);
    }

    private List<SmsMessage> fetchSmsList(){
        List<SmsMessage> smsMessages = new ArrayList<>();
        Cursor cursor = contentResolver.query(Telephony.Sms.Inbox.CONTENT_URI,null,null,null,Telephony.Sms.Inbox.DEFAULT_SORT_ORDER);
        while (cursor!=null && cursor.moveToNext()){
            String address = cursor.getString(cursor.getColumnIndex(Telephony.Sms.Inbox.ADDRESS));
            String body = cursor.getString(cursor.getColumnIndex(Telephony.Sms.Inbox.BODY));
            Log.d(TAG, "fetchSmsList: Address :"+address);
            Log.d(TAG, "fetchSmsList: Body :"+body);
            Log.d(TAG, "fetchSmsList: ---------------------------");
            SmsMessage smsMessage = new SmsMessage();
            smsMessage.setBody(body);
            smsMessage.setAddress(address);
            smsMessages.add(smsMessage);
        }
        cursor.close();
        return  smsMessages;
    }
}
