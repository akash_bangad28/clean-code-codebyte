package com.mangoapps.mangoappscodebyte;

import android.app.Application;
import android.content.Context;

import com.mangoapps.data.AppRepositoryImpl;
import com.mangoapps.data.local.LocalPropertiesDao;
import com.mangoapps.data.local.LocalPropertiesImpl;
import com.mangoapps.data.provider.contact.ContactDao;
import com.mangoapps.data.provider.contact.ContactDaoImpl;
import com.mangoapps.data.provider.phonelog.PhoneLogDao;
import com.mangoapps.data.provider.phonelog.PhoneLogDaoImpl;
import com.mangoapps.data.provider.sms.SmsDao;
import com.mangoapps.data.provider.sms.SmsDaoImpl;
import com.mangoapps.domain.repository.AppRepository;

/**
 * Created by Akash Bangad on 10/04/2018.
 */

public class MyApplication extends Application {


    public static final String SHARED_PREF_NAME = "com.mangoapps.mangoappscodebyte.mypref";

    @Override
    public void onCreate() {
        super.onCreate();
    }


    //Todo - Replace with dependency Injection **Dagger**
    public AppRepository getAppRepository(){
        ContactDao contactDao = new ContactDaoImpl(getApplicationContext().getContentResolver());
        PhoneLogDao phoneLogDao = new PhoneLogDaoImpl(getApplicationContext().getContentResolver());
        SmsDao smsDao = new SmsDaoImpl(getApplicationContext().getContentResolver());
        LocalPropertiesDao localPropertiesDao = new LocalPropertiesImpl(getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE));
        return new AppRepositoryImpl(contactDao, phoneLogDao, smsDao, localPropertiesDao);
    }
}
