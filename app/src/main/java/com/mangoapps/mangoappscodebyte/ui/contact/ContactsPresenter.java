package com.mangoapps.mangoappscodebyte.ui.contact;

import com.mangoapps.domain.interactors.contact.GetContactsListUseCase;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Akash Bangad on 09/04/2018.
 */

public class ContactsPresenter implements ContactsContract.Presenter {

    private final GetContactsListUseCase getContactsListUseCase;
    private final ContactsContract.View view;

    public ContactsPresenter(GetContactsListUseCase getContactsListUseCase, ContactsContract.View view) {
        this.getContactsListUseCase = getContactsListUseCase;
        this.view = view;
    }

    @Override
    public void fetchContactsList() {
        getContactsListUseCase
                .execute()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(contactList -> {
                    if (contactList == null){
                        view.showError("Sorry, unable to fetch contacts");
                    }else if (contactList.size() == 0){
                        view.showError("There are no contacts to display");
                    }else {
                        view.showContactsList(contactList);
                    }
                });

    }
}
