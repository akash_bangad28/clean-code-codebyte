package com.mangoapps.mangoappscodebyte.ui.dashboard;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.mangoapps.data.local.LocalPropertiesImpl;
import com.mangoapps.mangoappscodebyte.MyApplication;
import com.mangoapps.mangoappscodebyte.R;
import com.mangoapps.mangoappscodebyte.ui.sms.SmsFragment;
import com.mangoapps.mangoappscodebyte.ui.contact.ContactsFragment;
import com.mangoapps.mangoappscodebyte.ui.phonelog.PhoneLogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashboardActivity extends AppCompatActivity {

    
    public static final String TAG = DashboardActivity.class.getSimpleName();
    
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private FragmentManager fragmentManager;
    private MyApplication application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        setInitialFragment();
        setNavigationView();

    }


    public void setInitialFragment(){
        fragmentManager = getSupportFragmentManager();
        application = (MyApplication) getApplication();
        int currentPage = application.getAppRepository().getCurrentPage();
        switch (currentPage){
            case LocalPropertiesImpl.CONTACTS_PAGE:
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame, new ContactsFragment())
                        .commit();
                getSupportActionBar().setTitle(getString(R.string.contact_title));
                break;
            case LocalPropertiesImpl.CALL_PAGE:
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame, new SmsFragment())
                        .commit();
                getSupportActionBar().setTitle(getString(R.string.call_log_title));
                break;

            case LocalPropertiesImpl.SMS_PAGE:
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame, new PhoneLogFragment())
                        .commit();
                getSupportActionBar().setTitle(getString(R.string.sms_title));
                break;
        }

    }

    public void setNavigationView() {
        navigationView.setNavigationItemSelectedListener(item -> {
            item.setChecked(true);
            drawerLayout.closeDrawers();
            switch (item.getItemId()) {
                case R.id.nav_contacts:
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, new ContactsFragment())
                            .commit();
                    application.getAppRepository().setCurrentPage(LocalPropertiesImpl.CONTACTS_PAGE);
                    getSupportActionBar().setTitle(getString(R.string.contact_title));

                    break;
                case R.id.nav_sms:
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, new SmsFragment())
                            .commit();
                    application.getAppRepository().setCurrentPage(LocalPropertiesImpl.SMS_PAGE);
                    getSupportActionBar().setTitle(getString(R.string.sms_title));

                    break;
                case R.id.nav_call_logs:
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, new PhoneLogFragment())
                            .commit();
                    application.getAppRepository().setCurrentPage(LocalPropertiesImpl.CALL_PAGE);
                    getSupportActionBar().setTitle(getString(R.string.call_log_title));

                    break;
            }
            return true;
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
