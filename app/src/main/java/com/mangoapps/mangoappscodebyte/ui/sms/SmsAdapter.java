package com.mangoapps.mangoappscodebyte.ui.sms;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mangoapps.domain.model.SmsMessage;
import com.mangoapps.mangoappscodebyte.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Akash Bangad on 10/04/2018.
 */

public class SmsAdapter extends RecyclerView.Adapter<SmsAdapter.ViewHolder>{

    private List<SmsMessage> smsMessages;

    public SmsAdapter(List<SmsMessage> smsMessages) {
        this.smsMessages = smsMessages;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View singleRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_sms,parent,false);
        ViewHolder viewHolder = new ViewHolder(singleRow);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.address.setText(smsMessages.get(position).getAddress());
        holder.body.setText(smsMessages.get(position).getBody());
    }

    @Override
    public int getItemCount() {
        return smsMessages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.row_sms_address)
        TextView address;

        @BindView(R.id.row_sms_body)
        TextView body;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}