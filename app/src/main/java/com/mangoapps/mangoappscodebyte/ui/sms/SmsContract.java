package com.mangoapps.mangoappscodebyte.ui.sms;

import com.mangoapps.domain.model.SmsMessage;

import java.util.List;

/**
 * Created by Akash Bangad on 10/04/2018.
 */

public interface SmsContract {
    interface View{
        void showError(String message);
        void showSmsList(List<SmsMessage> smsMessageList);
    }

    interface Presenter{
        void fetchSmsList();
    }
}
