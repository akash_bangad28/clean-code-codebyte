package com.mangoapps.mangoappscodebyte.ui.phonelog;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mangoapps.domain.model.CallLog;
import com.mangoapps.mangoappscodebyte.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Akash Bangad on 09/04/2018.
 */

public class PhoneLogAdapter extends RecyclerView.Adapter<PhoneLogAdapter.ViewHolder> {

    private final List<CallLog> phoneLogs;
    private final Context context;

    public PhoneLogAdapter(List<CallLog> callLogs, Context context) {
        this.phoneLogs = callLogs;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View singleRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_phone_log, parent, false);
        ViewHolder viewHolder = new ViewHolder(singleRow);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.type.setText(phoneLogs.get(position).getType());
        holder.date.setText(getDate(phoneLogs.get(position).getDate()));
        if (phoneLogs.get(position).getName() == null) {
            holder.display.setText(phoneLogs.get(position).getNumber());
        } else {
            holder.display.setText(phoneLogs.get(position).getName());
        }
    }

    @Override
    public int getItemCount() {
        return phoneLogs.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.row_log_display_name_number)
        TextView display;

        @BindView(R.id.row_log_type)
        TextView type;

        @BindView(R.id.row_log_date)
        TextView date;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:"+phoneLogs.get(getAdapterPosition()).getNumber()));
                    context.startActivity(intent);
                }
            });
        }
    }

    private String getDate(String date){
        Date date1 = new Date(Long.valueOf(date));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
       return simpleDateFormat.format(date1);
    }
}
