package com.mangoapps.mangoappscodebyte.ui.phonelog;

import android.Manifest;
import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mangoapps.data.AppRepositoryImpl;
import com.mangoapps.data.provider.contact.ContactDao;
import com.mangoapps.data.provider.contact.ContactDaoImpl;
import com.mangoapps.data.provider.phonelog.PhoneLogDao;
import com.mangoapps.data.provider.phonelog.PhoneLogDaoImpl;
import com.mangoapps.data.provider.sms.SmsDao;
import com.mangoapps.data.provider.sms.SmsDaoImpl;
import com.mangoapps.domain.interactors.phonelog.GetPhoneLogUseCase;
import com.mangoapps.domain.model.CallLog;
import com.mangoapps.domain.repository.AppRepository;
import com.mangoapps.mangoappscodebyte.MyApplication;
import com.mangoapps.mangoappscodebyte.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Akash Bangad on 09/04/2018.
 */

public class PhoneLogFragment extends Fragment implements PhoneLogContract.View, SwipeRefreshLayout.OnRefreshListener {


    private static final int READ_CALL_LOGS_PERMISSION = 122;

    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.list_phone_logs)
    RecyclerView phoneLogRv;

    PhoneLogContract.Presenter presenter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_phonelog, container, false);
        ButterKnife.bind(this, root);
        swipeRefreshLayout.setOnRefreshListener(this);
        MyApplication application = (MyApplication) getActivity().getApplication();
        GetPhoneLogUseCase getPhoneLogUseCase = new GetPhoneLogUseCase(application.getAppRepository());
        presenter = new PhoneLogPresenter(getPhoneLogUseCase, this);
        return root;
    }






    @Override
    public void showError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showPhoneLog(List<CallLog> logs) {
        PhoneLogAdapter phoneLogAdapter = new PhoneLogAdapter(logs,getContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        phoneLogRv.setLayoutManager(linearLayoutManager);
        phoneLogRv.setAdapter(phoneLogAdapter);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if ( ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CALL_LOG)
                != PackageManager.PERMISSION_GRANTED){
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("Permissions Required");
            builder.setMessage("We require you to provide us the access to your call logs so as to work appropriately");
            builder.setPositiveButton("Accept", (dialogInterface, i) -> {
                dialogInterface.dismiss();
                requestPermissions(new String[]{ Manifest.permission.READ_CALL_LOG}, READ_CALL_LOGS_PERMISSION);
            });
            builder.setNegativeButton("Deny", (dialogInterface, i) -> dialogInterface.dismiss());
            builder.show();
        }else {
            presenter.fetchPhoneLogs();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case READ_CALL_LOGS_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    presenter.fetchPhoneLogs();
                } else {
                    Toast.makeText(getContext(), "Permission dined", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        Toast.makeText(getContext(), "Refresh", Toast.LENGTH_SHORT).show();
        presenter.fetchPhoneLogs();
    }
}
