package com.mangoapps.mangoappscodebyte.ui.sms;

import com.mangoapps.domain.interactors.sms.GetSmsListUseCase;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Akash Bangad on 10/04/2018.
 */

public class SmsPresenter implements SmsContract.Presenter {

    private final GetSmsListUseCase getSmsListUseCase;
    private final SmsContract.View view;

    public SmsPresenter(GetSmsListUseCase getSmsListUseCase, SmsContract.View view) {
        this.getSmsListUseCase = getSmsListUseCase;
        this.view = view;
    }

    @Override
    public void fetchSmsList() {
        getSmsListUseCase
                .execute()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(smsMessageList -> {
                    if (smsMessageList==null){
                        view.showError("Sorry unable to fetch your inbox");
                    }else if (smsMessageList.isEmpty()){
                        view.showError("Inbox is empty");
                    }else {
                        view.showSmsList(smsMessageList);
                    }
                });

    }
}
