package com.mangoapps.mangoappscodebyte.ui.contact;

import android.Manifest;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mangoapps.data.AppRepositoryImpl;
import com.mangoapps.data.provider.contact.ContactDao;
import com.mangoapps.data.provider.contact.ContactDaoImpl;
import com.mangoapps.data.provider.phonelog.PhoneLogDao;
import com.mangoapps.data.provider.phonelog.PhoneLogDaoImpl;
import com.mangoapps.domain.interactors.contact.GetContactsListUseCase;
import com.mangoapps.domain.model.Contact;
import com.mangoapps.domain.repository.AppRepository;
import com.mangoapps.mangoappscodebyte.MyApplication;
import com.mangoapps.mangoappscodebyte.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Akash Bangad on 09/04/2018.
 */

public class ContactsFragment extends Fragment implements ContactsContract.View, SwipeRefreshLayout.OnRefreshListener {

    private final int CONTACT_PERMISSION_REQUEST = 123;

    @BindView(R.id.list_contacts)
    RecyclerView contactsRv;

    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    ContactsContract.Presenter presenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_contacts, container, false);
        ButterKnife.bind(this, root);
        swipeRefreshLayout.setOnRefreshListener(this);
        MyApplication application = (MyApplication) getActivity().getApplication();
        GetContactsListUseCase getContactsListUseCase = new GetContactsListUseCase(application.getAppRepository());
        presenter = new ContactsPresenter(getContactsListUseCase, this);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //Replace with Dependency Injection
    }




    @Override
    public void showError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showContactsList(List<Contact> contactList) {
        ContactsAdapter contactsAdapter = new ContactsAdapter(contactList, getContext().getContentResolver(), new AlertDialog.Builder(getContext()), getContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        contactsRv.setLayoutManager(linearLayoutManager);
        contactsRv.setAdapter(contactsAdapter);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onResume() {
        super.onResume();

        int readContactsStatus = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS);
        int writeContactsStatus = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_CONTACTS);
        int callPhoneStatus = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);
        if ((readContactsStatus != PackageManager.PERMISSION_GRANTED) || (writeContactsStatus != PackageManager.PERMISSION_GRANTED) ||
                (callPhoneStatus != PackageManager.PERMISSION_GRANTED)) {

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(R.string.permission_required_title);
            builder.setMessage(R.string.rational_phone_permission);
            builder.setPositiveButton(R.string.action_accept, (dialogInterface, i) -> {
                dialogInterface.dismiss();
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS,
                        Manifest.permission.WRITE_CONTACTS, Manifest.permission.CALL_PHONE}, CONTACT_PERMISSION_REQUEST);
            });
            builder.setNegativeButton(R.string.action_deny, (dialogInterface, i) -> dialogInterface.dismiss());
            builder.show();
            // Permission is not granted
        } else {
            presenter.fetchContactsList();


        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CONTACT_PERMISSION_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    presenter.fetchContactsList();
                } else {
                    Toast.makeText(getContext(), R.string.denied_permission, Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        presenter.fetchContactsList();
    }
}
