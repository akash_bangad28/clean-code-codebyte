package com.mangoapps.mangoappscodebyte.ui.contact;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mangoapps.domain.model.Contact;
import com.mangoapps.mangoappscodebyte.R;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Akash Bangad on 09/04/2018.
 */

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {

    private final List<Contact> contacts;
    private final ContentResolver contentResolver;
    private final AlertDialog.Builder builder;
    private final Context context;

    public ContactsAdapter(List<Contact> contacts, ContentResolver contentResolver, AlertDialog.Builder builder, Context context) {
        this.contacts = contacts;
        this.contentResolver = contentResolver;
        this.builder = builder;

        this.context = context;
    }

    @NonNull
    @Override
    public ContactsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View singleRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_contact, parent, false);
        ViewHolder viewHolder = new ViewHolder(singleRow);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ContactsAdapter.ViewHolder holder, int position) {
        if (contacts.get(position).getName() != null)
            holder.name.setText(contacts.get(position).getName());
        if (contacts.get(position).getNumber() != null)
            holder.number.setText(contacts.get(position).getNumber());
        if (contacts.get(position).getPhoto() != null) try {
            holder.thumb.setImageBitmap(MediaStore.Images.Media.getBitmap(contentResolver, Uri.parse(contacts.get(position).getPhoto())));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    private void showContactDetails(Contact contact) {
        builder.setTitle(R.string.action);
        builder.setMessage(contact.getName());
        builder.setPositiveButton(R.string.btn_call, (dialogInterface, i) -> {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + contact.getNumber()));
            context.startActivity(intent);
        });
        builder.setNegativeButton(R.string.btn_sms, ((dialogInterface, i) -> {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", contact.getNumber(), null)));

        }));
        builder.create().show();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.contact_row_name)
        TextView name;

        @BindView(R.id.contact_row_number)
        TextView number;

        @BindView(R.id.contact_row_thumb)
        CircleImageView thumb;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(view -> showContactDetails(contacts.get(getAdapterPosition())));
        }
    }
}
