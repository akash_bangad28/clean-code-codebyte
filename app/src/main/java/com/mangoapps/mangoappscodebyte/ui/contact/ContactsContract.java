package com.mangoapps.mangoappscodebyte.ui.contact;

import com.mangoapps.domain.model.Contact;

import java.util.List;

/**
 * Created by Akash Bangad on 09/04/2018.
 */

public interface ContactsContract {

    interface View {
        void showError(String message);
        void showContactsList(List<Contact> contactList);
    }

    interface Presenter{
        void fetchContactsList();
    }
}
