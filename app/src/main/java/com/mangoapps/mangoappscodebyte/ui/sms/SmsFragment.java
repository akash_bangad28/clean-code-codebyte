package com.mangoapps.mangoappscodebyte.ui.sms;

import android.Manifest;
import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mangoapps.domain.interactors.sms.GetSmsListUseCase;
import com.mangoapps.domain.model.SmsMessage;
import com.mangoapps.mangoappscodebyte.MyApplication;
import com.mangoapps.mangoappscodebyte.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Akash Bangad on 10/04/2018.
 */

public class SmsFragment extends Fragment implements SmsContract.View, SwipeRefreshLayout.OnRefreshListener {


    private static final int READ_SMS_PERMISSION = 112;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.list_sms)
    RecyclerView smsRv;

    private SmsContract.Presenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_sms,container,false);
        ButterKnife.bind(this,root);
        MyApplication application = (MyApplication) getActivity().getApplication();
        GetSmsListUseCase getSmsListUseCase = new GetSmsListUseCase(application.getAppRepository());
        presenter = new SmsPresenter(getSmsListUseCase,this);
        swipeRefreshLayout.setOnRefreshListener(this);
        return root;
    }


    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        presenter.fetchSmsList();
    }

    @Override
    public void showError(String message){
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showSmsList(List<SmsMessage> smsMessageList) {
        SmsAdapter smsAdapter = new SmsAdapter(smsMessageList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        smsRv.setLayoutManager(linearLayoutManager);
        smsRv.setAdapter(smsAdapter);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if ( ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_SMS)
                != PackageManager.PERMISSION_GRANTED){
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("Permissions Required");
            builder.setMessage("We require you to provide us the access to your sms so as to work appropriately");
            builder.setPositiveButton("Accept", (dialogInterface, i) -> {
                dialogInterface.dismiss();
                requestPermissions(new String[]{ Manifest.permission.READ_SMS}, READ_SMS_PERMISSION);
            });
            builder.setNegativeButton("Deny", (dialogInterface, i) -> dialogInterface.dismiss());
            builder.show();
        }else {
            presenter.fetchSmsList();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case READ_SMS_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    presenter.fetchSmsList();
                } else {
                    Toast.makeText(getContext(), "Permission dined", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
