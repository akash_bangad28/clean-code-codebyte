package com.mangoapps.mangoappscodebyte.ui.phonelog;

import com.mangoapps.domain.model.CallLog;

import java.util.List;

/**
 * Created by Akash Bangad on 09/04/2018.
 */

public interface PhoneLogContract {
    interface View{
        void showError(String message);
        void showPhoneLog(List<CallLog> logs);
    }

    interface Presenter {
        void fetchPhoneLogs();
    }
}
