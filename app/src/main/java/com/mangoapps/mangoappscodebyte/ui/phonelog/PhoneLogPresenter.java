package com.mangoapps.mangoappscodebyte.ui.phonelog;

import com.mangoapps.domain.interactors.phonelog.GetPhoneLogUseCase;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Akash Bangad on 09/04/2018.
 */

public class PhoneLogPresenter implements PhoneLogContract.Presenter {

    private final GetPhoneLogUseCase getPhoneLogUseCase;
    private final PhoneLogContract.View view;

    public PhoneLogPresenter(GetPhoneLogUseCase getPhoneLogUseCase, PhoneLogContract.View view) {
        this.getPhoneLogUseCase = getPhoneLogUseCase;
        this.view = view;
    }

    @Override
    public void fetchPhoneLogs() {
        getPhoneLogUseCase
                .execute()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(logs -> {
                    if (logs == null) {
                        view.showError("Unable to fetch the logs");
                    } else if (logs.size() == 0) {
                        view.showError("No logs found");
                    } else {
                        view.showPhoneLog(logs);
                    }
                });
    }
}
