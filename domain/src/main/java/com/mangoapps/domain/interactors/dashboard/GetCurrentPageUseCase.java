package com.mangoapps.domain.interactors.dashboard;

import com.mangoapps.domain.repository.AppRepository;

/**
 * Created by Akash Bangad on 10/04/2018.
 */

public class GetCurrentPageUseCase {

    private final AppRepository appRepository;

    public GetCurrentPageUseCase(AppRepository appRepository) {
        this.appRepository = appRepository;
    }

    public int getCurrentPage(){
        return appRepository.getCurrentPage();
    }
}
