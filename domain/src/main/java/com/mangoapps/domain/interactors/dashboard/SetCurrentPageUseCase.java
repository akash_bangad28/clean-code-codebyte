package com.mangoapps.domain.interactors.dashboard;

import com.mangoapps.domain.repository.AppRepository;

/**
 * Created by Akash Bangad on 10/04/2018.
 */

public class SetCurrentPageUseCase {

    private final AppRepository appRepository;

    public SetCurrentPageUseCase(AppRepository appRepository) {
        this.appRepository = appRepository;
    }

    public void setCurrentPage(int page) {
        appRepository.setCurrentPage(page);
    }
}
