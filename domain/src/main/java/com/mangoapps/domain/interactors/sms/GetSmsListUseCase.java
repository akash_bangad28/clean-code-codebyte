package com.mangoapps.domain.interactors.sms;

import com.mangoapps.domain.interactors.type.SingleUseCase;
import com.mangoapps.domain.model.SmsMessage;
import com.mangoapps.domain.repository.AppRepository;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Akash Bangad on 10/04/2018.
 */

public class GetSmsListUseCase implements SingleUseCase<List<SmsMessage>> {

    private final AppRepository appRepository;

    public GetSmsListUseCase(AppRepository appRepository) {
        this.appRepository = appRepository;
    }

    @Override
    public Single<List<SmsMessage>> execute() {
        return appRepository.getSmsList();
    }
}
