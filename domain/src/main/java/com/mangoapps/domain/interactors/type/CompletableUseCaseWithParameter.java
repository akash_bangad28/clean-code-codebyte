package com.mangoapps.domain.interactors.type;


import io.reactivex.Completable;

public interface CompletableUseCaseWithParameter<P> {

    Completable execute(P parameter);
}
