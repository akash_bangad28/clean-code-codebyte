package com.mangoapps.domain.interactors.contact;

import com.mangoapps.domain.interactors.type.SingleUseCase;
import com.mangoapps.domain.model.Contact;
import com.mangoapps.domain.repository.AppRepository;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Akash Bangad on 08/04/2018.
 */

public class GetContactsListUseCase implements SingleUseCase<List<Contact>> {

    private final AppRepository appRepository;

    public GetContactsListUseCase(AppRepository appRepository) {
        this.appRepository = appRepository;
    }

    @Override
    public Single<List<Contact>> execute() {
        return appRepository.getContactList();
    }
}
