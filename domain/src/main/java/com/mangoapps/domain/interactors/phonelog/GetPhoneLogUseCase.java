package com.mangoapps.domain.interactors.phonelog;

import com.mangoapps.domain.interactors.type.SingleUseCase;
import com.mangoapps.domain.model.CallLog;
import com.mangoapps.domain.repository.AppRepository;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Akash Bangad on 09/04/2018.
 */

public class GetPhoneLogUseCase implements SingleUseCase<List<CallLog>> {

    private final AppRepository appRepository;

    public GetPhoneLogUseCase(AppRepository appRepository) {
        this.appRepository = appRepository;
    }

    @Override
    public Single<List<CallLog>> execute() {
        return appRepository.getCallLogs();
    }
}
