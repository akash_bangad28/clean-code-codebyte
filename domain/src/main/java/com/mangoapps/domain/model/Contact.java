package com.mangoapps.domain.model;

/**
 * Created by Akash Bangad on 08/04/2018.
 */

public class Contact {
    private String id;
    private String name;
    private String photo;
    private String thumb;
    private String number;


    public Contact(String id,String name, String photo, String number,String thumb){
        this.id = id;
        this.name = name;
        this.photo = photo;
        this.number = number;
        this.thumb = thumb;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
