package com.mangoapps.domain.model;

/**
 * Created by Akash Bangad on 10/04/2018.
 */

public class SmsMessage {

    private String address;
    private String body;

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
