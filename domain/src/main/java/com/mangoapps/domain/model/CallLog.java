package com.mangoapps.domain.model;

/**
 * Created by Akash Bangad on 09/04/2018.
 */

public class CallLog {
    private String name;
    private String number;
    private String date;
    private String type;

    public String getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getType() {
        return type;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setType(String type) {
        this.type = type;
    }
}
