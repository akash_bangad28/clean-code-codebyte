package com.mangoapps.domain.repository;

import com.mangoapps.domain.model.CallLog;
import com.mangoapps.domain.model.Contact;
import com.mangoapps.domain.model.SmsMessage;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Akash Bangad on 08/04/2018.
 */

public interface AppRepository {

    Single<List<Contact>> getContactList();

    Single<List<CallLog>> getCallLogs();

    Single<List<SmsMessage>> getSmsList();

    void setCurrentPage(int Page);

    int getCurrentPage();
}
